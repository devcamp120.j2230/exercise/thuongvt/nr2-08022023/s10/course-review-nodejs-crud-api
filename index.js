const express = require("express");// Khai báo thư vienj exp
const mongoose = require("mongoose");// Khai báo thư viện mongoose

// Khai báo router
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");

//Khai báo model
// const reviewModel = require("./app/models/reviewModel");
// const courseModel = require("./app/models/courseModel")

const app = express();

const prot = 3000;

// Để app có thể đọc được bodyjson
app.use(express.json());

// Kêt nối mongoodb
mongoose.connect('mongodb://localhost:27017/CRUD_Course',(error)=>{
    if (error) throw error;

    console.log('Successfully connected');
})

// sử dụng router
app.use("/", courseRouter);
app.use("/",reviewRouter);

app.listen(prot, ()=>{
    console.log("app listen on prot", prot)
});
