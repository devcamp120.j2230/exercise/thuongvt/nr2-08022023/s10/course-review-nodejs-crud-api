// khai báo mongoose 
const mongoose = require("mongoose");
// sử dụng model
const courseModel = require("../models/courseModel");

// tạo một dữ liệu mói
const createCourse = (req, res) => {
    //B1 thu thập dữ liệu
    // tạo một biến thu thập dữ liệu
    let body = req.body;
    console.log(body);
    //B2 Kiểm tra dữ liệu
    // chia các trường hợp các khả năng có thể có
    // nếu biến title không nhập hoặc biến title bằng rỗng sẽ báo lỗi
    if (body.title === undefined || body.title === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }
    // biến noStudent phải là số nguyên khác số không
    if (body.noStudent !== undefined && !(Number.isInteger(body.noStudent) && body.noStudent >= 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "No student is not valid!"
        })
    }

    //B3 Xử lý bài toán đặt ra
    //B3.1 chuẩn bị dữ liệu
    let newCourseData = {
        _id: mongoose.Types.ObjectId(),
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    }
    // sử dụng model tạo mới một dữ liệu
    courseModel.create(newCourseData, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "creat successfuly",
                newCourse: data
            })
        }
    })

}
// lấy toàn bộ dữ liệu
const getAllCourse = (req, res) => {
    //B1 thu thập dữ liệu
    //B2 kiểm tra dữ liệu
    //B3 Thực hiện bài toán
    courseModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(201).json({
                message: "successfuly load all data",
                Courses: data
            })
        }
    })
}
// Lấy dữ liệu theo ID
const getCourseById = (req, res) => {
    //B1 thu thập dữ liệu
    let Id = req.params.courseId;
    //B2 Kiểm tra dữ liệu 
    // Kiểm tra id
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'courseId is invaId'
        })
    }
    console.log(Id);
    // B3 Thực hiện bài toán
    courseModel.findById(Id, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "successfuly load data by ID",
                Course: data
            })
        }
    })
}
// Sửa dữ liệu theo id
const updateCourseById = (req, res) => {
    // B1 thu thập dữ liệu
    // tạo biến thu thập dữ liệu
    let Id = req.params.courseId;
    let body = req.body;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'courseId is invaId'
        })
    }
    console.log("Id của dữ liệu sửa là: ", Id);
    // chia các trường hợp các khả năng có thể có
    // nếu biến title không nhập hoặc biến title bằng rỗng sẽ báo lỗi
    if (body.title === undefined || body.title === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }
    // biến noStudent phải là số nguyên khác số không
    if (body.noStudent !== undefined && !(Number.isInteger(body.noStudent) && body.noStudent >= 0)) {
        return res.status(400).json({
            status: "Bad Request",
            message: "No student is not valid!"
        })
    }
    // B3 Gọi model thực hiện bài toán
    let updateCourse = {
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    }
    courseModel.findByIdAndUpdate(Id, updateCourse, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "successfuly updated data by ID",
                Course: data
            })
        }
    })
}
// Xóa dữ liệu theo Id
const deleteCourseById = (req,res)=>{
    // B1 thu thập dữ liệu
    // tạo biến thu thập dữ liệu
    let Id = req.params.courseId;
    //B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(Id)) {
        return res.status(400).json({
            message: 'courseId is invaId'
        })
    }
    console.log("Id của dữ liệu xoa là: ", Id);
    //B3 Gội model thực hiện bài toán
    courseModel.findByIdAndDelete(Id,(err,data)=>{
        if (err) {
            return res.status(500).json({
                message: err.message
            })
        }
        else {
            return res.status(200).json({
                message: "successfuly delete data by ID",
                Course: data
            })
        }
    })
}

//Export thư viện controller thành một modul
module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}