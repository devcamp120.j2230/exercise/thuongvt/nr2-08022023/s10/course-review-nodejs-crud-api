const getAllCourseMidlleware = (req,res,next)=>{
    console.log("Get all course ");
    next()
}

const getCourseMidlleware = (req,res,next)=>{
    console.log("Get a course ");
    next()
}

const postCourseMidlleware = (req,res,next)=>{
    console.log("post course ");
    next()
}

const putCourseMidlleware = (req,res,next)=>{
    console.log("put course ");
    next()
}

const deleteCourseMidlleware = (req,res,next)=>{
    console.log("delete course ");
    next()
}

module.exports = {
    getAllCourseMidlleware,
    getCourseMidlleware,
    postCourseMidlleware,
    putCourseMidlleware,
    deleteCourseMidlleware
}