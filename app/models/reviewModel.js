// improt thư viện mongo
const mongoose = require("mongoose");

// khai báo class schema của mongo
const schema = mongoose.Schema

// khai báo  review schema 
const reviewSchema = new schema({
    // trường Id có thể khai báo hoặc không
    _id: {
        type: mongoose.Types.ObjectId,
        require: true
    },
    // trường thứ 2 
    stars:{
        type: Number,
        default: 0
    },
    // trường thứ 3
    note :{
        type: String,
        require: false
    }, 

},{
    timestamps : true
});

 module.exports = mongoose.model("Review",reviewSchema)
