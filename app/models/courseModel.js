// import thư viện mongoose
const mongoose = require("mongoose");

// khai báo class schema của thư viện
const schema = mongoose.Schema;

// khai báo coureschema
const courseScheme = new schema({
    title:{
        type: String,
        require: true,
        unique: true
    },
    description:{
        type: String,
        require: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    reviews:[
        {
            type: mongoose.Types.ObjectId,
            ref: "Review"
        }
    ]
},{
    timestamps : true
})

module.exports = mongoose.model("course",courseScheme);
