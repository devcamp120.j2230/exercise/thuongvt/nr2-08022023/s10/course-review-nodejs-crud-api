//Khai báo thư viện express 
const express = require("express");

//Khai báo sử dụng middleware
const {
    getAllCourseMidlleware,
    getCourseMidlleware,
    postCourseMidlleware,
    putCourseMidlleware,
    deleteCourseMidlleware
} = require(`../middleware/courseMiddleware`)

// khai báo contrllers
const {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
} = require("../controllers/courseController")

//Tạo Router
const courseRouter = express.Router();

// sử dụng router
courseRouter.get("/course", getAllCourseMidlleware, getAllCourse);
courseRouter.get("/course/:courseId", getCourseMidlleware, getCourseById);
courseRouter.put("/course/:courseId", putCourseMidlleware, updateCourseById);
courseRouter.post("/course", postCourseMidlleware, createCourse);
courseRouter.delete("/course/:courseId", deleteCourseMidlleware, deleteCourseById);

module.exports = { courseRouter };

